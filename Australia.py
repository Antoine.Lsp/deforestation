import tkinter as tk
import sys
from tkinter import *
from random import *


# fenêtre principale
master = tk.Tk()

#variable
probab = sys.argv[4]
size = sys.argv[3]
rows = sys.argv[2]
cols = sys.argv[1]
colonne = int(rows)
ligne = int(cols)

# le Tableau à afficher
foret = [[0] * int(colonne+1) for _ in range(int(ligne+1))]
for j in range(int(colonne)):
    for i in range(int(ligne)):
        valeur=random()
        if valeur<float(probab):
            foret[j][i]=1
        else:
            foret[j][i]=0

# les 4 couleurs à utiliser
couleurs = {0: "white", 1: "green", 2: "grey", 3: "red"}

# dimensions du canevas
can_width = int(size)*int(colonne)
can_height = int(size)*int(ligne)

# création canevas
can = tk.Canvas(master, width=can_width, height=can_height)
can.grid()

def afficher(t):
    for j in range(len(t)):
        for i in range(len(t[j])):
            can.create_rectangle(i * int(size),
                                j * int(size),
                                i * int(size + size),
                                j * int(size + size),
                                fill = couleurs[foret[j][i]])
        i=0;

def modifierforet(evt):
    pos_x = int(evt.x / int(size))
    pos_y = int(evt.y / int(size))
    # inverser la valeur de l'élément cliqué
    if foret[pos_y][pos_x] == 1:
        foret[pos_y][pos_x] = 3
    elif foret[pos_y][pos_x] == 3:
        foret[pos_y][pos_x] = 1
    # ré-afficher le Tableau
    afficher(foret)
    
        
def propaganda():
    foret_next = [[0] * int(colonne) for _ in range(int(ligne))]
    for col in range(int(colonne)):
        for lig in range(int(ligne)):
            if foret[col][lig]== 1 :
                test = 0
                if(col!=col-1 and foret[col+1][lig]==3):
                    test+=1
                if(col!=0 and foret[col-1][lig]==3):
                    test+=1
                if(lig!=lig-1 and foret[col][lig+1]==3):
                    test+=1
                if(lig!=0 and foret[col][lig-1]==3):
                    test+=1
                if(test>=1):
                    foret_next[col][lig]=3
                else:
                    foret_next[col][lig]=1
            elif foret[col][lig]== 3 :
                foret_next[col][lig] = 2
            elif foret[col][lig]== 2 : 
                foret_next[col][lig] = 0
            elif foret[col][lig]== 0 : 
                foret_next[col][lig] = 0
    for col in range(int(colonne)):
        for lig in range(int(ligne)):
            foret[col][lig]=foret_next[col][lig]
    # ré-afficher le Tableau
    afficher(foret_next)
    master.after(1000,propaganda)

#-----------------------------------------------------
# programme
afficher(foret)
# binding de la fonction modifierforet sur le canevas
can.bind("<Button-1>", modifierforet)
# dimension boutton simuler
bouton=Button(master, text="Burn Baby, Burn", command=propaganda)
bouton.grid()
# boucle principale
master.mainloop()
